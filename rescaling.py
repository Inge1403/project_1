import numpy as np
import Leap_Frog as LF

def rescaling(h,L,X,P,Td,interval,rescale_steps):
    """This function applies a rescaling proces to a particle distribution where the particles are interacting by a interaction potential. 
    
    ---
    Input Parameters:
    X = positions of the atoms for n particles, in 3 dimensions and for a number of timesteps.
    P = momentum of the atoms for n particles, in 3 dimensions and for a number of timesteps.
    X and P already contain the initial conditions, at timestep is zero. 
    Td = desired temperature
    """
    
    n_particles = np.size(X[:,0,0])
    n_timesteps = np.size(X[0,0,:])
    
    for i in range(rescale_steps):
        t0 = i*interval
        t1 = t0 + interval + 1

        X_int = X[:,:,t0:t1]
        P_int = P[:,:,t0:t1]

        [X_int,P_int] = LF.Leap_Frog(h,L,X_int,P_int)
        lambda_factor = np.sqrt(((n_particles-1)*3*Td)/(np.sum(P_int[:,:,interval]**2)))
        P_int[:,:,interval] = lambda_factor*P_int[:,:,interval]

        X[:,:,t0:t1] = X_int
        P[:,:,t0:t1] = P_int
        
    T_check = np.sum(P[:,:,interval*rescale_steps]**2)/(3*(n_particles-1))
                    
    return [X,P,T_check]