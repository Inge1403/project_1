#import all important functions:
import nearest_neighbour as nn
import LJ 
import numpy as np

def En_Cons(X,P,L):
    """This function returns the Total Energy of the all the particles, the Kinetic energy of the particles and the Potential energy of the particles.
    ---
    Input Parameters:
    X = a matrix with the positions of all particles for all time steps
    P = a matrix with the momentum of all particles for all time steps
    L = box size
    """
    
    
    #declare array's to store the values for the potential energy (Vpot), the kinetic energy (T) and the total energy (E)
    n_particles = np.size(X[:,0,0])
    n_timesteps = np.size(X[0,0,:])
    rho = n_particles/(L**3)
    U = np.zeros(n_timesteps)
    T = np.zeros(n_timesteps)
    E = np.zeros(n_timesteps)

    for t in range(n_timesteps):
        for i in range(n_particles-1):
            # derive the distance to the nearest neighbour for timestep t, particle i and length of the box L
            dist = nn.nearest_neighbour(X[:,:,t],i,L)
            r = np.sqrt(dist[0]**2 + dist[1]**2 + dist[2]**2)
            r = np.delete(r,i,0)
            r = r[r<L/2]
    
            U_sum = np.sum(4*((1/r)**12 - (1/r)**6))
            #derive the potential energy for particle i and add it to the total potential energy for timestep t
            #LJ.LJ is the Lennard-Jones potential, d[0] is the radius between a particle and its nearest neighbour

            U[t] = U[t] + U_sum 
            
        #derive the kinetic energy for particle i and add it to the total kinetic energy for timestep t
        #V[i,0,t] gives the velocity of particle i in x at time t, V[i,1,t] gives the velocity of particle i in y at time t
        T[t] = 1/2*np.sum(P[:,:,t]**2)
        U_cutoff = 8/9*np.pi*rho*(n_particles-1)*((L/2)**(-9)-3*(L/2)**(-3))
        U[t] = U_cutoff + U[t]/2
        #derive the total energy for timestep t
        E[t] = T[t] + U[t] 
        
        #E_n = E/n_particles
        #T_n = T/n_particles
        #U_n = U/n_particles
        
        
    return [E,T,U]

