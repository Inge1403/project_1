import numpy as np
import nearest_neighbour as nn
import LJ
import auto_cor as ac
import matplotlib.pyplot as plt

def virial(X,L,Td,t_resc,tau = 1/2):
    """This function calculates the virial.
    Input paramters
    --------
    X: the position vector of the particles
    L: the size of the box
    Td: desired temperature
    t_resc: index from which is started the calculation
    tau: (optional) correlation time of the observable
    
    Returns
    --------
    sum_over_i: the virial for eacht timestep
    """
    n_particles = np.size(X[:,0,0])
    n_timesteps = np.size(X[0,0,:])
    
    rho = n_particles/(L**3)
    
    sum_over_i = np.zeros(n_timesteps-t_resc)
    for t in range(n_timesteps-t_resc):
        t_after_resc = t + t_resc
        for i in range(n_particles):
            d = nn.nearest_neighbour(X[:,:,t_after_resc],i,L)
            radius_ij = np.sqrt(d[0]**2+d[1]**2+d[2]**2)
            radius_ij = np.delete(radius_ij,i,0)
            
            dU_dr = LJ.dU_dr(radius_ij,i,L)
            sum_over_j = np.sum(radius_ij[radius_ij<L/2]*dU_dr)

            sum_over_i[t] = sum_over_i[t] + sum_over_j
    
    sample = int(np.rint(2*tau))
    mean_Virial = 1/2*np.mean(sum_over_i[::sample])
    return [sum_over_i]
    
def pressure(n,Td,L,mean_virial,rho):
    """Computes the pressure of the system
    Input parameters:
    -------
    n: number of particles in the system
    Td: desired temperature
    L: size of the system
    mean_virial: mean of the virial
    rho: density of the system
    
    Returns:
    -------
    Pressure: pressure of the system
    """
    Press_cutoff = (2*np.pi*rho)/(9*Td)*(16*(2/L)**(9)-2*(2/L)**(3))
    #This function returns the pressure in units                    
    Pressure = 1-1/(3*n)*mean_virial*1/2 + Press_cutoff #the mean virial is divided by two since in the computation of the virial all particles were counted twice.
    print('Pressure =',Pressure)
    return Pressure
    

        
    

    