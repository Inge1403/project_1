import numpy as np

def periodic_BC(X,L):
    """This function takes a location vector and applies periodic boundary conditions for a box with size L*L. It return the vector X
    inside the box.
    
    Input
    -----
    X: vector with particle positions x,y
    L: box size
    """
    s = X.shape[0]
    for i in range(s):
        k0 = np.floor(X[i,0]/L)
        X[i,0]=X[i,0]-k0*L
                
        k1= np.floor(X[i,1]/L)
        X[i,1]=X[i,1]-k1*L
        
        k2 = np.floor(X[i,2]/L)
        X[i,2]=X[i,2]-k2*L
    return X