import numpy as np
import init_grid as init
import LJ
import nearest_neighbour as nn
import periodic_BC as BC

def Euler(h,T,L,n,X,V,F):
    """This function implements the Euler method to find the positions of particles in a box interacting with the Lennard-Jones potential
    ---
    Input Parameters:
    h = length of a time step
    T = total number of time steps
    L = box size
    n = number of particles
    X = matrix of the positions of all particles for all time steps
    V = matrix of the velocities of all particles for all time steps
    F = matrix of the forces on all particles for all time steps
    """
    #declare some arrays which store all the values of the force, the positions and the velocity for all timesteps. We will need it when we want to plot
    #the results.

    #The important code. For each timestep the force on the particle is calculated and with this force the new x and v.
    for t in range(T-1):
        #calculate the forces acting on each particle
        for i in range(n):
            #derive the nearest neighbour and its properties for particle i
            d = nn.nearest_neighbour(X[:,:,t],i,L)
            #derive the force acting on particle i, in the direction of its nearest neighbour:
            F[i,:,t] = LJ.dLJ(d,i)

        #calculate the new postion and velocity for all particles
        #the position and velocity for the next timestep for particle i
        V[:,:,t+1] = V[:,:,t] + F[:,:,t]*h  
        X[:,:,t+1] = X[:,:,t] + V[:,:,t]*h
            
        #apply periodic boundary conditions
        X[:,0,t+1]=X[:,0,t+1]-np.floor(X[:,0,t+1]/L)*L              
        X[:,1,t+1]=X[:,1,t+1]-np.floor(X[:,1,t+1]/L)*L     
        X[:,2,t+1]=X[:,2,t+1]-np.floor(X[:,2,t+1]/L)*L
            
    return [X,V,F]

        