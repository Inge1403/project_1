from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt

def func(x, a, b, c):
    return a * np.exp(-b * x) + c

def auto_cor(A):
    """ This function returns the correlation time in terms of indices of the observable A.
    Parameters:
    ------
    A: observable for which the correlation time is going to be calculated
    
    Returns:
    ------
    chi_norm: normalized auto correlation function
    """
    n_max = np.size(A)
    t = np.arange(n_max)

    A_mat = np.array([A[i]*A for i in range (n_max)])
    sum_1 = np.array([np.sum(np.diagonal(A_mat,offset = -i)) for i in range(n_max)])
    sum_2 = np.array([np.sum(A_mat[i:n_max-1,i:n_max-1]) for i in range(n_max)])
    chi = 1/(n_max-t)*sum_1[t]-(1/(n_max-t))**2*sum_2[t]
    chi_norm = chi/chi[0]
    plt.figure()
    plt.plot(t[0:int(n_max/10)],chi_norm[0:int(n_max/10)]) #The plots are cutoff 1/10 before the end of the array to cut off the high increase in the end. 
    plt.xlabel('Lag')
    plt.ylabel('Normalized Intensity Auto Correlation')
    plt.title('Auto Correlation Function')
    
    return chi_norm

def fit_auto_cor(chi,right):
    """ This function returns a plot of the fitted exponent to a part of the auto correlation function. From this fitted exponent
    the correlation time tau is retrieved.
    Parameters:
    -----
    chi: (normalized)auto correlation function
    right: defines the right boundary of the interval between zero and 'right' in which the fit is executed
    
    Returns:
    -----
    tau: correlation time
    """
    t = np.arange(np.size(chi))
    [popt, pcov]=curve_fit(func, t[0:right], chi[0:right])
    plt.figure()
    plt.plot(t[0:int(np.size(chi)/10)],func(t[0:int(np.size(chi)/10)],popt[0],popt[1],popt[2]))   
    plt.plot(t[0:int(np.size(chi)/10)], chi[0:int(np.size(chi)/10)]) #The plots are cutoff 1/10 before the end of the array to cut off the high increase in the end. 
    plt.xlabel('Lag')
    plt.ylabel('Normalized Intensity Auto Correlation')
    plt.title('Auto Correlation Function and Fit')
    plt.gca().legend(('Fit','Auto_Cor'),loc = 1)
    tau = 1/popt[1]
    
    return tau

def mean_std(A,tau,name = 'A'):
    """ This function returns the mean value and the standard deviation for an uncorrelated observable. 
    Parameters:
    -----
    A: observable
    tau: measure for the sample size, retrieved from fitting the auto correlation function.
    name: (optional) default = 'A' name of the observable
    
    Returns:
    -----
    A_mean: mean value of the observable
    sigma_A: standard deviation
    """
    A_uncor = A[::int(2*tau)]
    A_mean= np.mean(A_uncor)
    sigma_A = np.sqrt(1/(np.size(A_uncor)-1)*(np.mean(np.square(A_uncor))-np.mean(A_uncor)**2))
    print(name,'=',A_mean,'(',A_mean-sigma_A,',',A_mean+sigma_A,')')
    return [A_mean,sigma_A]