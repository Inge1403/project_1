import numpy as np

def nearest_neighbour(x,i,L):
    """This function derives the radius and the distance in x,y,z coordinates with respect to the nearest neighbour of particle number j
    in a box with size L*L*L. It applies minimal image convention to solve for particles near the edge of the box.
    
    Input
    -----
    x: vector with particle positions x,y,z
    i: The number of the certain particle 
    L: box size
    """
    # (xi-xj+1/2L)mod L -1/2L implements the periodic b.c. for the distance'between the particle's i and j. This function gives the distance vector from
    # xi towards xj. This direction of the vector is important to derive the right force on particle i.
    mx = x[i,0] - x[:,0]
    my = x[i,1] - x[:,1]
    mz = x[i,2] - x[:,2]
    X = np.mod(mx+1/2*L,L)-1/2*L
    Y = np.mod(my+1/2*L,L)-1/2*L
    Z = np.mod(mz+1/2*L,L)-1/2*L
    
    #Using X and Y we can derive the radius between the other particles and particle i. Finding the minimal radius, where we correct for the fact that   the    radius between particle i itself is always zero, we find the index of the nearest neighbour. 
    #R2 = X**2+Y**2+Z**2
    #R2[np.argmin(R2)] = 2*L
    #near = np.argmin(R2)
    
    #Using the index we for the nearest neighbour the radius and x,y position from the neighbour to particle i can be found and returned. 
    distance = [X, Y, Z]
    return distance

