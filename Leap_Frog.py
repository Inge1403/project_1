import numpy as np
import nearest_neighbour as nn
import periodic_BC as BC
import LJ
import init_grid as init

def Leap_Frog(h,L,X,P):
    """This function implements the Leap Frog numerical integration method to determine the positions X and velocities P of particles in a box interacting with a Lennard Jones potential. it returns the positions and the velocities
    ---
    Input Parameters:
    h = length of a time step
    L = box size
    X = matrix of the positions of all particles for all time steps
    P = matrix of momenta of all particles for all time steps
    """
    
    #h is the timestep
    #T is the total Time
    #L is the length of the box
    #n is the number of particles
    #X,P,F are the position, momentum and force matrices. These contain already the initial conditions
    n_particles = np.size(X[:,0,0])  
    n_timesteps = np.size(X[0,0,:])
    
    for t in range(n_timesteps-1):
        F = np.zeros([n_particles,3])
        for i in range(n_particles):
            d = nn.nearest_neighbour(X[:,:,t],i,L)
            #dLJ has as output the force, so a minus sign is not needed
            F[i,:] = LJ.dLJ(d,i)

        #for P, the timestep t is an intermediate step between n and n = 1 (n = -1/2,1/2,3/2...), according to the Verlet algorithm; 
        #For the force F the index is the same as for X, so integer numbers (n = 0,1,2 ...)
        #at t = 0, P starts at n = 1/2 and F,X start at n = 1. 
        
        P[:,:,t+1] = P[:,:,t] + F*h
        
        #for X, the timestep t is index n in the Verlet algortihm
        
        X[:,:,t+1] = X[:,:,t] + P[:,:,t+1]*h

            
        #below the periodic boundary conditions are implement
        X[:,0,t+1]=X[:,0,t+1]-np.floor(X[:,0,t+1]/L)*L
        X[:,1,t+1]=X[:,1,t+1]-np.floor(X[:,1,t+1]/L)*L
        X[:,2,t+1]=X[:,2,t+1]-np.floor(X[:,2,t+1]/L)*L
                        
    return [X,P]
            
