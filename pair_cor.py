import nearest_neighbour as nn
import numpy as np
import bisect 
import matplotlib.pyplot as plt

def pair_cor(Xf,Lf,rescf,dTf,dR):
    """This function returns a histogram with the average radial distance of the particles with respect to one reference particle and a figure which gives
    the pair distribution function.
    ----
    Input parameters:
    Xf = position matrix for all particles and for all t
    Lf = box size
    rescf = the number of times the velocity will be rescaled
    dTf = the number of timesteps after which the velocity will be rescaled
    dR = the interval in which the particles are counted or the bin size
    """

    [n,dim,T] = np.shape(Xf)
    
    t1 = rescf*dTf+1 #gives the first index after rescaling
    
    ref_particle = 0
    r_max = 0.75*Lf # gives the maximum radius for which de pair_cor is calculated
    n_timesteps=int((T-t1))
    
    #Compute particle locations for the copies of the box
    plus=int(np.ceil(r_max/Lf)) # number of boxes to append in x+,x-,y+,y-,z+,z- direction 
    n_boxes=(2*plus+1)**3 # total number of boxes
    print("amount boxes = ",n_boxes) 
    
    plus_value_x = np.zeros(np.shape(Xf))
    min_value_x = np.zeros(np.shape(Xf))
    copiesx12=Xf
    for i in range(plus): 
        plus_value_x[:,0,:] = Lf*(i+1)
        min_value_x[:,0,:] = -Lf*(i+1)
        x_plus = Xf+plus_value_x
        x_min = Xf+min_value_x
        copiesx1 = np.append(copiesx12,x_plus,axis=0)
        copiesx12 = np.append(copiesx1,x_min,axis=0)
        
    plus_value_y = np.zeros(np.shape(copiesx12))
    min_value_y = np.zeros(np.shape(copiesx12))
    copiesx12y12=copiesx12
    for i in range(plus): 
        plus_value_y[:,1,:] = Lf*(i+1)
        min_value_y[:,1,:] = -Lf*(i+1)
        y_plus = copiesx12+plus_value_y
        y_min =copiesx12+min_value_y
        copiesx12y1 = np.append(copiesx12y12,y_plus,axis=0)
        copiesx12y12 = np.append(copiesx12y1,y_min,axis=0)

    plus_value_z = np.zeros(np.shape(copiesx12y12))
    min_value_z = np.zeros(np.shape(copiesx12y12))
    copiesx12y12z12=copiesx12y12
    for i in range(plus): 
        plus_value_z[:,2,:] = Lf*(i+1)
        min_value_z[:,2,:] = -Lf*(i+1)
        z_plus = copiesx12y12+plus_value_z
        z_min =copiesx12y12+min_value_z
        copiesx12y12z1 = np.append(copiesx12y12z12,z_plus,axis=0)
        copiesx12y12z12 = np.append(copiesx12y12z1,z_min,axis=0)
    
    #Compute the distance between the particles
    bins = np.arange(0, r_max, dR)
    counts = np.zeros([n_timesteps,int(np.size(bins)-1)])
    
    for j in range(n):
        XYZ=copiesx12y12z12-copiesx12y12z12[j,:,:]
        R = np.sqrt(XYZ[:,0,:]**2+XYZ[:,1,:]**2+XYZ[:,2,:]**2)
        R = np.delete(R,(j),axis=0) #remove the reference particle from the list
        counts_particle = np.zeros([n_timesteps,int(np.size(bins)-1)])
        for i in range(n_timesteps):
            counts_particle[i,:], bins = np.histogram(R[:,t1+i], bins,range=(0,r_max),density=False)
            
        counts=counts+counts_particle
    
    counts = counts/2 #to compensate for the double counting of particle pairs 
    counts_mean = np.mean(counts, axis=0)
    plt.figure(1)
    plt.bar(dR*np.arange(np.size(counts_mean)),counts_mean,width =dR,align='edge')
    plt.xlabel('r')
    plt.ylabel('Counts')
    plt.title('Histogram of average counted particles in an interval dr = %.3f' %(dR))
   
    r = np.arange(0.01,bins[-1],0.01)
    g = n_r = np.zeros([np.size(r)])

    for i in range(np.size(r)):
        c=int(bisect.bisect_right(bins, r[i])-1)
        n_r[i]=counts_mean[c]
            
    
    g=(2*Lf**3*n_r)/(4*np.pi*(r**2)*dR*(n*(n-1)))

    plt.figure(2)
    plt.plot(r,g) 
    plt.xlabel('r')
    plt.ylabel('g(r)')
    plt.title('Pair correlation function')