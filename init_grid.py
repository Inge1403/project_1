import numpy as np
import matplotlib.pyplot as plt
import cube_root as cr
from scipy.stats import maxwell
from mpl_toolkits.mplot3d import Axes3D

def init_grid(n,rho):
    """This function returns a cube grid for the initial positions of the particles. All positions are altered with a small and a 
    random deviation 
    to prevent multiple nearest neighbours. Returns a (n,3) array with the initial x,y,z positions for all particles.
    
    Parameters:
    ----
    n: number of particles (must be a multple of 4 and n/4 must be a cube integer)
    rho: particle density 
    
    Returns:
    ----
    x: (n,3) matrix with the initial x,y,z location for all n particles
    L: box size
    """
    if n%4 !=0:
        print('ERROR: n must be a multiple of 4!')
    elif cr.is_perfect_cube(n/4) is False:
        print('ERROR: n/4 must be a cube integer')

    else:
        n_unit = 4 #number of particles per unit cell
        c_unit = int(n/n_unit) #number of unit cells
        L = np.cbrt(n/rho) # size of the simulation box
        h = np.zeros([c_unit,3])
        ns = int(np.rint((c_unit)**(1/3))) #number of unit cells in a row
        d = L/(ns) #distance between two fcc unit cells
        p = np.zeros([ns,1])

        #define unit cell:
        a1=[0,0,0]
        a2=[0,1/2,1/2]
        a3=[1/2,0,1/2]
        a4=[1/2,1/2,0]
        
        #define a cubic grid which correspond to the 'basic' lattice points
        for i in range(ns):
            p[i] =L/(ns)*i
            p = np.reshape(p,[ns])

        for i in range(ns):
            p[i] =L/(ns)*i
            p = np.reshape(p,[ns])

        for i in range(ns):
            h[ns*i:(ns)*(i+1),0]= p[:]
            h[ns*i:(ns)*(i+1),1]= p[i]

        for i in range(ns):
            h[ns**2*i:ns**2*(i+1),0:2]=h[0:ns**2,0:2] 
            h[ns**2*i:ns**2*(i+1),2]= p[i]
        
        #add the three other lattice points to every cell 
        
        x = np.zeros([n,3])
        
        for i in range(c_unit):
            x[4*i]=h[i,:]
            x[4*i+1]=h[i,:]+np.array(a2)*(L/ns)
            x[4*i+2]=h[i,:]+np.array(a3)*(L/ns)
            x[4*i+3]=h[i,:]+np.array(a4)*(L/ns)
            
        #plot figure         
        fig = plt.figure()        
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(x[:,0], x[:,1], x[:,2], zdir='z')
        plt.show()

        return [x, L]
    
def init_velocity(x_init,T):
    """This function generates an initial velocity using the Maxwell distribution. 
    ---
    Input parameters:
    h = particle positions in the box
    T = desired temperature of the system
    """
    
    #Get the number of particles and the d
    n = np.size(x_init[:,0])
    dim = np.size(x_init[0,:])
    
    #
    vx = maxwell.rvs(size=n)
    vx = vx-np.mean(vx)
    vy = maxwell.rvs(size=n)
    vy = vy-np.mean(vy)
    vz = maxwell.rvs(size=n)
    vz = vz-np.mean(vz)
    
    #Get the velocities from the variables given by the maxwell distribution. This can be done using x^2 = (mv^2/kT), and non-
    #dimensionalisation. 
    v = [vx, vy, vz]
    v = np.sqrt(3*T)*np.transpose(v)
    
    #Make a plot of the velocity distribution. The plot gives insight in how accurate the distribution is. 
    vt= np.sqrt(vx**2+vy**2+vz**2)
    #T_check = np.sum(vt**2)/(n-1)
    fig, ax = plt.subplots(1, 1)
    ax.hist(vt, bins =30,  density=True, histtype='stepfilled', alpha=0.2)
    plt.show()

    return v
    

    
    
    
    