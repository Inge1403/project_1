import numpy as np
import nearest_neighbour as nn

def LJ(dist,ref_particle):
    """This function implements the Lennard Jones Potential. It returns the potential energy of a particle which is a distance r from 
    the center of the other particle
    
    Input
    -----
    d: distances between particle i and all other particles
    i: index of particle i
    """
    
    r = np.sqrt(dist[0]**2 + dist[1]**2 + dist[2]**2)
    r = np.delete(r,ref_particle,0)
    #r = r[r<L/2]
    
    U = 4*((1/r)**12 - (1/r)**6)
    
    U_sum = np.sum(U)
    return U_sum

def dLJ(dist,ref_particle):
    """This function implements the derivative of the Lennard Jones potential with respect to the radius between two particles. It returns the force of all particles on the reference patricle. 
    ---
    Input Parameters:
    dist = an array with the distances from the reference particle to all other particles in the box, in x,y,z coordinates
    ref_particle= the index of the reference particle"""
    
    #d is a combination of 3 arrays. d[0] is the x-direction, up to d[2] which is the z-direction. 
    #The arrays contain the distances from particle i to its (N-1) neighbours in x,y,z coordinates. 
    
    #declare an empty matrix wherethe calculations of the forces in the x,y,z directions will be stored
    F = np.zeros(np.size(dist))
    
    #calculate the radius from particle i to all other particles. Give a value to the radius from particle i to itself to avoid dividing by 0. 
    r = np.sqrt(dist[0]**2 + dist[1]**2 + dist[2]**2)
    r[ref_particle] = np.max(dist)

    #calculate the derivative of the Lennard Jones potential using the radius
    dU_dr = -24*(2*(1/r)**13-(1/r)**7)

    #calculate all forces on particle i including the directions in x,y,z. The last part is done by multiplying dU_dr with d/r.
    #The force of the particle on itself is not important and should be zero. 
    F = -dU_dr/r*dist
    F = np.delete(F,ref_particle,1)
    #Summing all the forces gives the total force on particle i. 
    F_sum = [np.sum(F[0,:]), np.sum(F[1,:]), np.sum(F[2,:])]

    return F_sum

def dU_dr(r,ref_particle,L):
    """This function returns just the derivative of the Lennard Jones potential as a function of the radius between the reference particle and all other particles, within a sphere with radius L/2.
    ---
    Input parameters:
    r = an array with the radius between the reference particle and all other particles, except is own.
    ref_particle = the index of the reference particle.
    L = the box size
    """
    #calculate the radius from particle i to all other particles. Give a value to the radius from particle i to itself to avoid dividing by 0. 
    #r = np.sqrt(dist[0]**2 + dist[1]**2 + dist[2]**2)
    r = r[r<L/2]
    
    dU_dr = -24*(2*(1/r)**13 - (1/r)**7)
    
    return dU_dr


    