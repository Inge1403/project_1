import numpy as np

def dLJ(d,i):
    #d is a combination of 3 arrays. d[0] is the x-direction, up to d[2] which is the z-direction. 
    #The arrays contain the distances from particle i to its (N-1) neighbours in x,y,z coordinates. 
    
    #declare an empty matrix wherethe calculations of the forces in the x,y,z directions will be stored
    F = np.zeros(np.size(d))
    
    #calculate the radius from particle i to all other particles. Give a value to the radius from particle i to itself to avoid dividing by 0. 
    r = np.sqrt(d[0]**2 + d[1]**2 + d[2]**2)
    r[i] = np.max(d)

    #calculate the derivative of the Lennard Jones potential using the radius
    dU_dr = -24*(2*(1/r)**13-(1/r)**7)

    #calculate all forces on particle i including the directions in x,y,z. The last part is done by multiplying dU_dr with d/r.
    #The force of the particle on itself is not important and should be zero. 
    F = -dU_dr/r*d
    F[:,i] = 0
    #Summing all the forces gives the total force on particle i. 
    F_sum = [np.sum(F[0,:]), np.sum(F[1,:]), np.sum(F[2,:])]

    return F_sum